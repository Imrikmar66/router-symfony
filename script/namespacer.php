#!/usr/bin/php
<?php 
if(empty($argv[1])) {
    echo "Base folder needed";
    return;
}
if(!is_dir( $argv[1] )) {
    echo "A dir path is needed";
    return;
}

runIntoFolder( $argv[1] );

function runIntoFolder( $folder ) {
    $folders = scandir( $folder );
    array_splice( $folders, 0, 2 );

    foreach ($folders as $path) {

        $path = $folder . '/' . $path;

        if( is_dir( $path ) ) {

            runIntoFolder( $path );

        }
        else if( is_file( $path ) && pathinfo( $path )['extension'] == 'php' ) {

            $content = file_get_contents( $path );

            if( strpos( $content, 'namespace' ) > -1 ) continue;

            $namespace = '';
            $parts = explode('/', $path);
            for( $i=0; $i<count($parts)-1; $i++ ) {
                $namespace .= ucfirst( $parts[$i] ). '\\';
            }
            $namespace = rtrim( $namespace, '\\' );
            
            $content = str_replace( '<?php', '<?php' . PHP_EOL . "namespace ${namespace};" . PHP_EOL, $content );
            file_put_contents( $path, $content );

        }

    }
}