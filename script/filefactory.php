#!/usr/bin/php
<?php 
if(empty($argv[1])) {
    echo "Base folder needed";
    return;
}
if(empty($argv[2])) {
    echo "Max iteration needed";
    return;
}
if(!is_dir( $argv[1] )) {
    echo "A dir path is needed";
    return;
}
$max_iteration = intval( $argv[2] );
if(!$max_iteration || $max_iteration > 6) {
    echo "A number between 1 - 6 is required";
    return;
}

$path = $argv[1];
generateDirFile( $path, $max_iteration, true );

function generateDirFile( $dir, $max_iteration, $only_dir = false ) {

    
    $str = generateRandomString();
    $path = $dir . '/' . $str;
    if( !$only_dir && rand(0, 1) ) {
        $file = $path . '.php';
        file_put_contents( $file, '<?php' . PHP_EOL . 'class ' . $str . '{' . PHP_EOL . '}' );
    }
    else {
        $max_iteration > 0 ? $max_iteration-- : null;
        mkdir( $path );
        for( $i=0; $i<$max_iteration; $i++ ){
            generateDirFile( $path, $max_iteration );
        }
    }

}

function generateRandomString($length = 10) {
    $characters = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $char = $characters[rand(0, $charactersLength - 1)];
        while( $i==0 && intval($char) ){
            $char = $characters[rand(0, $charactersLength - 1)];
        }
        $randomString .= $char;
    }
    return $randomString;
}