<?php 
require_once './vendor/autoload.php';

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

try{

    // looks inside *this* directory
    $fileLocator = new FileLocator([__DIR__."/app/routes"]);
    $loader = new YamlFileLoader($fileLocator);
    $routes = $loader->load('routes.yaml');
    // Init RequestContext object
    $context = new RequestContext();
    $context->fromRequest(Request::createFromGlobals());
    
    // Init UrlMatcher object
    $matcher = new UrlMatcher($routes, $context);
    
    // Find the current route
    $parameters = $matcher->match($context->getPathInfo());

    $class = $parameters['controller'];
    $method = $parameters['method'];
    (new $class)->$method();


} catch( \Exception $e ) {
    echo $e;
}